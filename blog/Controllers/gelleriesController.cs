﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using blog.Data;
using blog.Models;

namespace blog.Controllers
{
    public class gelleriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public gelleriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: gelleries
        public async Task<IActionResult> Index()
        {
            return View(await _context.gelleries.ToListAsync());
        }

        // GET: gelleries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gellery = await _context.gelleries
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gellery == null)
            {
                return NotFound();
            }

            return View(gellery);
        }

        // GET: gelleries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: gelleries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,photourl")] gellery gellery)
        {
            if (ModelState.IsValid)
            {
                _context.Add(gellery);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(gellery);
        }

        // GET: gelleries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gellery = await _context.gelleries.FindAsync(id);
            if (gellery == null)
            {
                return NotFound();
            }
            return View(gellery);
        }

        // POST: gelleries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,photourl")] gellery gellery)
        {
            if (id != gellery.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gellery);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!gelleryExists(gellery.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(gellery);
        }

        // GET: gelleries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gellery = await _context.gelleries
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gellery == null)
            {
                return NotFound();
            }

            return View(gellery);
        }

        // POST: gelleries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var gellery = await _context.gelleries.FindAsync(id);
            _context.gelleries.Remove(gellery);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool gelleryExists(int id)
        {
            return _context.gelleries.Any(e => e.Id == id);
        }
    }
}
